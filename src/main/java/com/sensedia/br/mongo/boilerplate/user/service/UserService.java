package com.sensedia.br.mongo.boilerplate.user.service;

import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.NotFoundException;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.AmqpPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.ApplicationPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.RepositoryPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
@Transactional
@Validated
public class UserService implements ApplicationPort {

    private final RepositoryPort repository;
    private final AmqpPort amqpPort;

    @Autowired
    public UserService(RepositoryPort repository, AmqpPort amqpPort){
        this.repository = repository;
        this.amqpPort = amqpPort;
    }

    @Override
    public User create(@Valid @NotNull User user) {
        repository.save(user);
        amqpPort.notifyUserCreation(user);

        return user;
    }

    @Override
    public void delete(@NotNull String id) {
        User user = findById(id);
        user.disable();

        repository.save(user);
        amqpPort.notifyUserDeletion(user);
    }

    @Override
    public User update(@Valid @NotNull User userForUpdate, @NotNull String id) {
        User user = findById(id);

        if (user.changeNameAndEmail(userForUpdate.getName(), userForUpdate.getEmail())) {
            repository.save(user);
            amqpPort.notifyUserUpdate(user);
        }


        return user;
    }

    @Override
    public User findById(@NotNull String id) {
        return repository.findById(id)
                .orElseGet(() -> { throw new NotFoundException("User not found"); });
    }

    @Override
    public UserSearchResponse findAll(@Valid @NotNull UserSearch userSearch) {
        return repository.findAll(userSearch);
    }
}
