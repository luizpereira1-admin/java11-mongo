package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository;

import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;

public interface AdvancedUserSearch {

    UserSearchResponse findAll(UserSearch userSearch);
}
