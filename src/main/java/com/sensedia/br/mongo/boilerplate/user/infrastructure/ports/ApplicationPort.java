package com.sensedia.br.mongo.boilerplate.user.infrastructure.ports;

import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {
    User create(@Valid @NotNull User user);

    void delete(@NotNull String id);

    User update(@Valid @NotNull User user, @NotNull String id);

    User findById(@NotNull String id);

    UserSearchResponse findAll(@Valid @NotNull UserSearch userSearch);
}
