package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;

public interface Resolver<T extends Throwable> {
    DefaultErrorResponse getErrorResponse(T e);
}
