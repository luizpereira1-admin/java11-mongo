package com.sensedia.br.mongo.boilerplate.user.infrastructure.ports;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;

public interface AmqpPort {

    void notifyUserCreation(User user);

    void notifyUserDeletion(User user);

    void notifyUserOperationError(DefaultErrorResponse errorResponse);

    void notifyUserUpdate(User user);
}
