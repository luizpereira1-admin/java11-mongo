package com.sensedia.br.mongo.boilerplate.user.infrastructure.http;

import com.sensedia.br.mongo.boilerplate.commons.converters.InstantConverter;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchBuild;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.mappers.UserMapper;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.ApplicationPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.HEADER_ACCEPT_RANGE;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.HEADER_CONTENT_RANGE;
import static java.lang.String.valueOf;

@RestController
@RequestMapping("/users")
public class UserController {
    private final ApplicationPort userApplication;
    private final UserMapper userMapper;
    private final InstantConverter instantConverter;

    @Autowired
    public UserController(ApplicationPort userApplication, UserMapper userMapper, InstantConverter instantConverter) {
        this.userApplication = userApplication;
        this.userMapper = userMapper;
        this.instantConverter = instantConverter;
    }

    @RolesAllowed({ "admin", "user" })
    @GetMapping
    public ResponseEntity<List<UserDto>> getAll(
            @RequestParam(value = "status", required = false) final String status,
            @RequestParam(value = "name", required = false) final String name,
            @RequestParam(value = "email", required = false) final String email,
            @RequestParam(value = "created_at_start", required = false) final String createdAtStart,
            @RequestParam(value = "created_at_end", required = false) final String createdAtEnd,
            @RequestParam(value = "sort", required = false, defaultValue = "name") String sort,
            @RequestParam(value = "sort_type", required = false, defaultValue = "asc") String sortType,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "limit", required = false) Integer limit
    ) {
        UserSearch userSearch = UserSearchBuild.builder()
                .status(status)
                .name(name)
                .email(email)
                .createdAtStart(instantConverter.toInstant(createdAtStart))
                .createdAtEnd(instantConverter.toInstant(createdAtEnd))
                .sort(sort)
                .sortType(sortType)
                .page(page)
                .limit(limit)
                .build();

        UserSearchResponse userSearchResponse = userApplication.findAll(userSearch);

        List<UserDto> response = userMapper.toUserDtos(userSearchResponse.getUsers());

        return ResponseEntity.ok()
                .header(HEADER_CONTENT_RANGE, valueOf(userSearchResponse.getTotal()))
                .header(HEADER_ACCEPT_RANGE, valueOf(userSearchResponse.getMaximumLimitPerPage()))
                .body(response);
    }

    @RolesAllowed("admin")
    @PostMapping
    public ResponseEntity<UserDto> create(@RequestBody UserCreationDto userCreation) {
        User user = userApplication.create(userCreation.toUser());

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(user.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @RolesAllowed({ "admin", "user" })
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> get(@PathVariable String id) {
        User user = userApplication.findById(id);

        UserDto userResponse = userMapper.toUserDto(user);

        return ResponseEntity.ok(userResponse);
    }

    @RolesAllowed("admin")
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable String id, @Valid @RequestBody UserUpdateDto userUpdateDto) {
        userApplication.update(userUpdateDto.toUser(), id);
    }

    @RolesAllowed("admin")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        userApplication.delete(id);
    }
}
