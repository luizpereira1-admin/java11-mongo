package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

@Service
public class AccessDeniedExceptionResolver implements Resolver<AccessDeniedException> {

    @Override
    public DefaultErrorResponse getErrorResponse(AccessDeniedException e) {
        return new DefaultErrorResponse(HttpStatus.UNAUTHORIZED, e.getMessage());
    }
}
