package com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp;

import com.sensedia.br.mongo.boilerplate.commons.beans.BeanValidator;
import com.sensedia.br.mongo.boilerplate.commons.errors.resolvers.ExceptionResolver;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp.config.BindConfig;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp.config.BrokerInput;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDeletionDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.AmqpPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.ApplicationPort;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BrokerInput.class)
public class AmqpUserInbound {
    private final ApplicationPort applicationPort;
    private final AmqpPort amqpPort;
    private final ExceptionResolver exceptionResolver;

    public AmqpUserInbound(
            ApplicationPort applicationPort,
            AmqpPort amqpPort,
            ExceptionResolver exceptionResolver
    ) {
        this.applicationPort = applicationPort;
        this.amqpPort = amqpPort;
        this.exceptionResolver = exceptionResolver;
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_USER_CREATION_REQUESTED)
    public void subscribeExchangeUserCreationRequested(UserCreationDto userCreationDto) {
        try {
            applicationPort.create(userCreationDto.toUser());
        } catch (Exception e) {
            amqpPort.notifyUserOperationError(exceptionResolver.solve(e).addOriginalMessage(userCreationDto));
        }
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_USER_DELETION_REQUESTED)
    public void subscribeExchangeUserDeletionRequested(UserDeletionDto userDeletionDto) {
        try {
            applicationPort.delete(userDeletionDto.getId());
        } catch (Exception e) {
            amqpPort.notifyUserOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(userDeletionDto));
        }
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_USER_UPDATE_REQUESTED)
    public void subscribeExchangeUserUpdateRequested(UserUpdateDto userUpdateDto) {
        try {
            BeanValidator.validate(userUpdateDto);

            applicationPort.update(userUpdateDto.toUser(), userUpdateDto.getId());
        } catch (Exception e) {
            amqpPort.notifyUserOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(userUpdateDto));
        }
    }
}
