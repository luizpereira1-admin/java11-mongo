package com.sensedia.br.mongo.boilerplate.user.infrastructure.mappers;

import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(UserDecoratorMapper.class)
public interface UserMapper {
    User toUser(UserCreationDto userCreationDto);

    User toUser(UserUpdateDto userUpdateDto);

    UserDto toUserDto(User user);

    List<UserDto> toUserDtos(List<User> users);
}
