package com.sensedia.br.mongo.boilerplate.user.domain;

import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

@Document(collection = "users")
public class User {
    @Id
    private String id;

    @NotNull
    private String name;

    @NotNull
    @Email
    private String email;

    private UserStatus status;

    private Instant createdAt;

    private Instant updatedAt;

    public User() {
    }

    public User(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.status = UserStatus.ACTIVE;
        this.createdAt = Instant.now();
        this.updatedAt = Instant.now();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public UserStatus getStatus() {
        return status;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public boolean changeNameAndEmail(String name, String email) {
        if (this.name.equals(name) && this.email.equals(email)) {
            return false;
        }

        this.name = name;
        this.email = email;
        this.updatedAt = Instant.now();

        return true;
    }

    public void disable() {
        if (this.status == UserStatus.DISABLE) {
            throw new BadRequestException("User is already disabled");
        }

        this.status = UserStatus.DISABLE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id)
                && Objects.equals(name, user.name)
                && Objects.equals(email, user.email)
                && status == user.status
                && Objects.equals(createdAt, user.createdAt)
                && Objects.equals(updatedAt, user.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, status, createdAt, updatedAt);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("email", email)
                .append("status", status)
                .append("createdAt", createdAt)
                .append("updatedAt", updatedAt)
                .toString();
    }
}
