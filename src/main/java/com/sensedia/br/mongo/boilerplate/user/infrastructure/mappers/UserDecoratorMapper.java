package com.sensedia.br.mongo.boilerplate.user.infrastructure.mappers;

import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class UserDecoratorMapper implements UserMapper {

    @Autowired
    @Qualifier("delegate")
    private UserMapper delegate;

    @Override
    public User toUser(UserUpdateDto userUpdateDto) {
        return delegate.toUser(userUpdateDto);
    }
}
