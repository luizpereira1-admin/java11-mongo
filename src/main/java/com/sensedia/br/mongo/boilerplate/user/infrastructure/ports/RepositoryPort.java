package com.sensedia.br.mongo.boilerplate.user.infrastructure.ports;

import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.AdvancedUserSearch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryPort extends CrudRepository<User, String>, AdvancedUserSearch {}
