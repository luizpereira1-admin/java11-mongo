package com.sensedia.br.mongo.boilerplate.user.domain;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private User user;
    private Faker faker;
    private String id;
    private String name;
    private String email;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault());

    @BeforeEach
    void setUp() {
        this.faker = new Faker();

        this.id = this.faker.internet().uuid();
        this.name = this.faker.name().fullName();
        this.email = this.faker.internet().safeEmailAddress();

        this.user = new User(this.id, this.name, this.email);
    }

    @Test
    void whenInstantiatingANewUserTheDataMustBeReturnedCorrectly() {
        Instant now = Instant.now();

        assertEquals(this.id, this.user.getId());
        assertEquals(this.name, this.user.getName());
        assertEquals(this.email, this.user.getEmail());
        assertEquals(UserStatus.ACTIVE, this.user.getStatus());
        assertEquals(FORMATTER.format(now), FORMATTER.format(this.user.getCreatedAt()));
        assertEquals(FORMATTER.format(now), FORMATTER.format(this.user.getUpdatedAt()));
    }

    @Test
    void whenTheNameAndEmailIsChangedTheUserIsUpdatedInTheNameAndEmailInformed() {
        String newName = this.faker.name().fullName();
        String newEmail = this.faker.internet().safeEmailAddress();

        this.user.changeNameAndEmail(newName, newEmail);

        assertEquals(newName, this.user.getName());
        assertEquals(newEmail, this.user.getEmail());
    }

    @Test
    void whenDisabledTheStatusChangesToDisabledForTheUser() {
        this.user.disable();

        assertEquals(UserStatus.DISABLE, this.user.getStatus());
    }

    @Test
    void ifTheUserIsAlreadyDisabledAndIsDisabledAgainThrowABadRequestException() {
        this.user.disable();

        assertThrows(BadRequestException.class, () -> this.user.disable());
    }
}
