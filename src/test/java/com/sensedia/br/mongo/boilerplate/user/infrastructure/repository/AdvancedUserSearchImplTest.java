package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.PreConditionException;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.Sort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.SortType;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AdvancedUserSearchImplTest {

    private AdvancedUserSearch advancedUserSearch;
    private Faker faker;
    private List<User> users;

    private static final int MAXIMUM_LIMIT = 100;
    private static final int DEFAULT_LIMIT = 10;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.faker = new Faker();

        this.users = List.of(
                Mockito.mock(User.class),
                Mockito.mock(User.class),
                Mockito.mock(User.class)
        );

        MongoTemplate mongoTemplate = Mockito.mock(MongoTemplate.class);

        Mockito.when(mongoTemplate.count(Mockito.any(Query.class), Mockito.any(Class.class))).thenReturn(3L);
        Mockito.when(mongoTemplate.find(Mockito.any(Query.class), Mockito.any(Class.class))).thenReturn(this.users);

        this.advancedUserSearch = new AdvancedUserSearchImpl(mongoTemplate);
        ReflectionTestUtils.setField(this.advancedUserSearch, "maximumLimit", MAXIMUM_LIMIT);
        ReflectionTestUtils.setField(this.advancedUserSearch, "defaultLimit", DEFAULT_LIMIT);
    }

    @Test
    void findAllWithBasicSearchInformationNotThrowsExceptionAndReturnUserSearchResponse() {
        UserSearch search = new UserSearch();
        search.setPage(1);
        search.setSortType(SortType.ASC);
        search.setSort(Sort.NAME);
        search.setLimit(this.faker.number().numberBetween(1, 10));

        UserSearchResponse response = this.advancedUserSearch.findAll(search);

        assertEquals(this.users.size(), response.getUsers().size());
        assertEquals(this.users.size(), response.getTotal());
        assertEquals(MAXIMUM_LIMIT, response.getMaximumLimitPerPage());
    }

    @Test
    void findAllWithComplexInformationNotThrowsExceptionAndReturnUserSearchResponse() {
        UserSearch search = new UserSearch();
        search.setPage(1);
        search.setSortType(SortType.ASC);
        search.setSort(Sort.NAME);
        search.setLimit(this.faker.number().numberBetween(1, 10));
        search.setEmail(this.faker.internet().safeEmailAddress());
        search.setName(this.faker.name().fullName());
        search.setStatus(UserStatus.ACTIVE);
        search.setCreatedAtEnd(Instant.now());
        search.setCreatedAtStart(Instant.now());

        UserSearchResponse response = this.advancedUserSearch.findAll(search);

        assertEquals(this.users.size(), response.getUsers().size());
        assertEquals(this.users.size(), response.getTotal());
        assertEquals(MAXIMUM_LIMIT, response.getMaximumLimitPerPage());
    }

    @Test
    void findAllThrowsPreConditionExceptionIfLimitInformedGreaterThanMaximumLimitConfigured() {
        UserSearch search = new UserSearch();
        search.setPage(1);
        search.setSortType(SortType.ASC);
        search.setSort(Sort.NAME);
        search.setLimit(MAXIMUM_LIMIT + 1);

        assertThrows(PreConditionException.class, () -> this.advancedUserSearch.findAll(search));
    }
}
