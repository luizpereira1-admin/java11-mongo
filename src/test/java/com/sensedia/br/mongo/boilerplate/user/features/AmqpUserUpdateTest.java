package com.sensedia.br.mongo.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.io.IOException;

import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class AmqpUserUpdateTest extends AbstractUserTest {

    @BeforeEach
    public void setup() throws IOException {
        repository.deleteAll();
        loadDatabase();
    }

    @Test
    @DisplayName("I want to update a user with success")
    public void updateUserSuccessfully() throws IOException {
        UserUpdateDto userUpdateDto = new UserUpdateDto(
                USER_ID_VALID,
                "Thiago Costa",
                "thiago.costa@sensedia.com"
        );

        Message<UserUpdateDto> message =
                MessageBuilder.withPayload(userUpdateDto).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserUpdateRequested().send(message);

        // DATABASE VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("thiago.costa@sensedia.com");
        assertThat(user.getName()).isEqualTo("Thiago Costa");
        assertThat(user.getStatus()).isEqualTo(UserStatus.ACTIVE);
        assertThat(user.getCreatedAt()).isNotNull();
        assertThat(user.getUpdatedAt()).isNotNull();

        // NOTIFICATION VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserUpdated());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(userResponse.getEmail()).isEqualTo("thiago.costa@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Thiago Costa");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.ACTIVE.toString());
        assertThat(userResponse.getCreatedAt()).isNotNull();
        assertThat(userResponse.getUpdatedAt()).isNotNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserUpdate");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");
    }

    @Test
    @DisplayName("I want to update a user without email")
    public void updateUserWithoutEmail() throws IOException {
        UserUpdateDto userUpdateDto = new UserUpdateDto(
                USER_ID_VALID,
                "Thiago Costa",
                null
        );

        Message<UserUpdateDto> message =
                MessageBuilder.withPayload(userUpdateDto).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserUpdateRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserUpdateDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userUpdateDto);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("email é obrigatório ou está no formato inválido.");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(user.getName()).isEqualTo("Usuário 01");


        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }

    @Test
    @DisplayName("I want to update a user without name")
    public void updateUserWithoutName() throws IOException {
        UserUpdateDto userUpdateDto = new UserUpdateDto(
                USER_ID_VALID,
                null,
                "thiago.costa@sensedia.com"
        );

        Message<UserUpdateDto> message =
                MessageBuilder.withPayload(userUpdateDto).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserUpdateRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserUpdateDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userUpdateDto);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("name é obrigatório ou está no formato inválido.");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(user.getName()).isEqualTo("Usuário 01");


        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }

    @Test
    @DisplayName("I want to create a user with invalid email")
    public void updateUserWithInvalidEmail() throws IOException {
        UserUpdateDto userUpdateDto = new UserUpdateDto(
                USER_ID_VALID,
                "Thiago Costa",
                "thiago.costa.sensedia.com"
        );

        Message<UserUpdateDto> message =
                MessageBuilder.withPayload(userUpdateDto).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserUpdateRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserUpdateDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userUpdateDto);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("email deve ser um endereço de e-mail bem formado");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(user.getName()).isEqualTo("Usuário 01");


        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }
}
