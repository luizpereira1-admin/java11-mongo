package com.sensedia.br.mongo.boilerplate.user.infrastructure.dto;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserUpdateDtoTest {

    private UserUpdateDto userUpdateDto;
    private String id;
    private String name;
    private String email;

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.id = faker.internet().uuid();
        this.name = faker.name().fullName();
        this.email = faker.internet().safeEmailAddress();

        this.userUpdateDto = new UserUpdateDto(this.id, this.name, this.email);
    }

    @Test
    void getIdReturnStringIdInformedInInstantiation() {
        assertEquals(this.id, this.userUpdateDto.getId());
    }

    @Test
    void getNameReturnStringNameInformedInInstantiation() {
        assertEquals(this.name, this.userUpdateDto.getName());
    }

    @Test
    void getEmailReturnStringNameInformedInInstantiation() {
        assertEquals(this.email, this.userUpdateDto.getEmail());
    }

    @Test
    void toUserReturnUserWithInformedData() {
        User user = this.userUpdateDto.toUser();

        assertEquals(this.id, user.getId());
        assertEquals(this.name, user.getName());
        assertEquals(this.email, user.getEmail());
    }
}
