package com.sensedia.br.mongo.boilerplate.user.infrastructure.http;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.converters.InstantConverter;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.mappers.UserMapper;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.ApplicationPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.HEADER_ACCEPT_RANGE;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.HEADER_CONTENT_RANGE;

class UserControllerTest {

    @Mock
    HttpServletRequest request;

    private UserController userController;
    private Faker faker;

    @BeforeEach
    void setUp() {
        this.faker = new Faker();
        MockitoAnnotations.openMocks(this);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        UserSearchResponse userSearchResponse = Mockito.mock(UserSearchResponse.class);
        ApplicationPort applicationPort = Mockito.mock(ApplicationPort.class);
        UserMapper userMapper = Mockito.mock(UserMapper.class);
        InstantConverter instantConverter = Mockito.mock(InstantConverter.class);

        Mockito.when(userSearchResponse.getUsers()).thenReturn(List.of(Mockito.mock(User.class)));
        Mockito.when(userSearchResponse.getTotal()).thenReturn(1);
        Mockito.when(userSearchResponse.getMaximumLimitPerPage()).thenReturn(10);

        Mockito.when(applicationPort.findAll(Mockito.any(UserSearch.class))).thenReturn(userSearchResponse);
        Mockito.when(applicationPort.create(Mockito.any(User.class))).thenReturn(Mockito.mock(User.class));
        Mockito.when(applicationPort.findById(Mockito.anyString())).thenReturn(Mockito.mock(User.class));
        Mockito.when(applicationPort.update(Mockito.any(User.class), Mockito.anyString())).thenReturn(Mockito.mock(User.class));

        Mockito.when(userMapper.toUserDtos(Mockito.anyList())).thenReturn(List.of(Mockito.mock(UserDto.class)));
        Mockito.when(userMapper.toUserDto(Mockito.any(User.class))).thenReturn(Mockito.mock(UserDto.class));

        this.userController = new UserController(applicationPort, userMapper, instantConverter);
    }

    @Test
    void getAllMustGenerateTheStatusCodeTwoHundredWithTheHeadersAndBodyDefined() {
        ResponseEntity<List<UserDto>> response = this.userController.getAll(null, null, null,
                null, null, null, null, null, null);

        assertEquals("10", Objects.requireNonNull(response.getHeaders().get(HEADER_ACCEPT_RANGE)).get(0));
        assertEquals("1", Objects.requireNonNull(response.getHeaders().get(HEADER_CONTENT_RANGE)).get(0));

        assertEquals(HttpStatus.OK, response.getStatusCode());

        Objects.requireNonNull(response.getBody()).forEach(Assertions::assertNotNull);
    }

    @Test
    void createMustGenerateTheStatusCodeAndLocationAndBodyNull() {
        ResponseEntity<UserDto> response = this.userController.create(new UserCreationDto(
                this.faker.name().fullName(),
                this.faker.internet().safeEmailAddress()
        ));

        assertEquals("/", Objects.requireNonNull(response.getHeaders().get("Location")).get(0));
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void getMustGenerateTheStatusCodeAndBody() {
        ResponseEntity<UserDto> response = this.userController.get(this.faker.internet().uuid());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    void whenRunningUpdateNotThrowsException() {
        try {
            this.userController.update(this.faker.internet().uuid(), new UserUpdateDto(
                    null,
                    this.faker.name().fullName(),
                    this.faker.internet().safeEmailAddress()
            ));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningDeleteNotThrowsException() {
        try {
            this.userController.delete(this.faker.internet().uuid());
        } catch (Exception exception) {
            fail();
        }
    }
}
