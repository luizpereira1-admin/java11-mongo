package com.sensedia.br.mongo.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.io.IOException;

import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class AmqpUserCreationTest extends AbstractUserTest {
    @BeforeEach
    public void setup() {
        repository.deleteAll();
    }

    @Test
    @DisplayName("I want to create a user with success")
    public void createUserSuccessfully() throws IOException {
        UserCreationDto userCreation = new UserCreationDto(
                "Usuário 01",
                "usuario01@sensedia.com"
        );

        Message<UserCreationDto> message =
                MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserCreationRequested().send(message);

        // DATABASE VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(user.getName()).isEqualTo("Usuário 01");
        assertThat(user.getStatus()).isEqualTo(UserStatus.ACTIVE);
        assertThat(user.getCreatedAt()).isNotNull();
        assertThat(user.getUpdatedAt()).isNotNull();

        // NOTIFICATION VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserCreated());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(userResponse.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Usuário 01");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.ACTIVE.toString());
        assertThat(userResponse.getCreatedAt()).isNotNull();
        assertThat(userResponse.getUpdatedAt()).isNotNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserCreation");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");
    }

    @Test
    @DisplayName("I want to create a user without email")
    public void createUserWithoutEmail() throws IOException {
        UserCreationDto userCreation = new UserCreationDto(
                "Usuário 01",
                null
        );

        Message<UserCreationDto> message =
                MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

        brokerInput.subscribeUserCreationRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserCreationDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("email é obrigatório ou está no formato inválido.");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        assertThat(repository.findAll()).hasSize(0);

        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }

    @Test
    @DisplayName("I want to create a user without name")
    public void createUserWithoutName() throws IOException {
        UserCreationDto userCreation = new UserCreationDto(
                null,
                "usuario01@sensedia.com"
        );

        Message<UserCreationDto> message =
                MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

        brokerInput.subscribeUserCreationRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserCreationDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("name é obrigatório ou está no formato inválido.");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        assertThat(repository.findAll()).hasSize(0);

        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }

    @Test
    @DisplayName("I want to create a user with invalid email")
    public void createUserWithInvalidEmail() throws IOException {
        UserCreationDto userCreation = new UserCreationDto(
                "Usuário 01",
                "usuario.01.sensedia.com"
        );

        Message<UserCreationDto> message =
                MessageBuilder.withPayload(userCreation).setHeader(APP_ID_HEADER_NAME, "app-test").build();

        brokerInput.subscribeUserCreationRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserCreationDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userCreation);
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST.value());
        assertThat(response.getTitle()).isEqualTo(BAD_REQUEST.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("email deve ser um endereço de e-mail bem formado");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        assertThat(repository.findAll()).hasSize(0);

        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }
}
