package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

class DateTimeParseExceptionResolverTest {

    private DateTimeParseExceptionResolver dateTimeParseExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.dateTimeParseExceptionResolver = new DateTimeParseExceptionResolver();
        ReflectionTestUtils.setField(this.dateTimeParseExceptionResolver, "invalidFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        DateTimeParseException exception = Mockito.mock(DateTimeParseException.class);
        Mockito.when(exception.getMessage()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.dateTimeParseExceptionResolver.getErrorResponse(exception);

        assertEquals("error error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}
