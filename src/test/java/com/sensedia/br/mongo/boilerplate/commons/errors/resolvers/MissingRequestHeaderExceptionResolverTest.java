package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.bind.MissingRequestHeaderException;

import static org.junit.jupiter.api.Assertions.*;

class MissingRequestHeaderExceptionResolverTest {

    private MissingRequestHeaderExceptionResolver missingRequestHeaderExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.missingRequestHeaderExceptionResolver = new MissingRequestHeaderExceptionResolver();
        ReflectionTestUtils.setField(this.missingRequestHeaderExceptionResolver, "missingFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        MissingRequestHeaderException exception = Mockito.mock(MissingRequestHeaderException.class);
        Mockito.when(exception.getHeaderName()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.missingRequestHeaderExceptionResolver.getErrorResponse(exception);

        assertEquals("error error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}
