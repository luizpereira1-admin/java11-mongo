package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search;

import com.sensedia.br.mongo.boilerplate.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserSearchResponseTest {

    private List<User> users;
    private UserSearchResponse userSearchResponse;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.users = List.of(
                Mockito.mock(User.class),
                Mockito.mock(User.class),
                Mockito.mock(User.class)
        );

        this.userSearchResponse = new UserSearchResponse(
                this.users,
                this.users.size(),
                100
        );
    }

    @Test
    void getUsersReturnSameSizeOfInformed() {
        assertEquals(this.users.size(), this.userSearchResponse.getUsers().size());
    }

    @Test
    void getTotalReturnSameSizeOfList() {
        assertEquals(this.users.size(), this.userSearchResponse.getTotal());
    }

    @Test
    void getMaximumLimitPerPageReturnSameOfInformed() {
        assertEquals(100, this.userSearchResponse.getMaximumLimitPerPage());
    }
}
