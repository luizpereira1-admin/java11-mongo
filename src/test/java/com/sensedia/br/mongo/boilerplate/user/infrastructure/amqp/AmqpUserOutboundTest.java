package com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp.config.BrokerOutput;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.mappers.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import static org.junit.jupiter.api.Assertions.*;

class AmqpUserOutboundTest {

    private AmqpUserOutbound amqpUserOutbound;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        MessageChannel channel = Mockito.mock(MessageChannel.class);
        BrokerOutput output = Mockito.mock(BrokerOutput.class);
        UserMapper userMapper = Mockito.mock(UserMapper.class);

        Mockito.when(channel.send(Mockito.any(Message.class))).thenReturn(true);
        Mockito.when(output.publishUserCreated()).thenReturn(channel);
        Mockito.when(output.publishUserUpdated()).thenReturn(channel);
        Mockito.when(output.publishUserDeleted()).thenReturn(channel);
        Mockito.when(output.publishUserOperationError()).thenReturn(channel);
        Mockito.when(userMapper.toUserDto(Mockito.any(User.class))).thenReturn(Mockito.mock(UserDto.class));

        this.amqpUserOutbound = new AmqpUserOutbound(output, userMapper);
    }

    @Test
    public void whenRunningNotifyUserCreationNotThrowsException() {
        try {
            this.amqpUserOutbound.notifyUserCreation(Mockito.mock(User.class));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    public void whenRunningNotifyUserUpdateNotThrowsException() {
        try {
            this.amqpUserOutbound.notifyUserUpdate(Mockito.mock(User.class));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    public void whenRunningNotifyUserDeletionNotThrowsException() {
        try {
            this.amqpUserOutbound.notifyUserDeletion(Mockito.mock(User.class));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    public void whenRunningNotifyUserOperationErrorNotThrowsException() {
        try {
            this.amqpUserOutbound.notifyUserOperationError(Mockito.mock(DefaultErrorResponse.class));
        } catch (Exception exception) {
            fail();
        }
    }
}