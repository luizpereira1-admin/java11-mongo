package com.sensedia.br.mongo.boilerplate.user.infrastructure.dto;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDeletionDtoTest {

    private UserDeletionDto userDeletionDto;
    private String id;

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.id = faker.internet().uuid();
        this.userDeletionDto = new UserDeletionDto(this.id);
    }

    @Test
    void getIdReturnIdInformed() {
        assertEquals(this.id, this.userDeletionDto.getId());
    }
}
