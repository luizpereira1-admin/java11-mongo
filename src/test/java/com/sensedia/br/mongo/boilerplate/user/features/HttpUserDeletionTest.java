package com.sensedia.br.mongo.boilerplate.user.features;

import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class HttpUserDeletionTest extends AbstractUserTest{

    @BeforeEach
    public void setup() throws IOException {
        repository.deleteAll();
        loadDatabase();
    }

    @Test
    @DisplayName("I want delete a user with success")
    public void deleteUserSuccessfully() throws IOException {
        ResponseEntity<?> response = request.exchange(
                "/users/{id}",
                HttpMethod.DELETE,
                HttpEntity.EMPTY,
                Object.class,
                USER_ID_VALID
        );

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.NO_CONTENT.value());

        // DATABASE VALIDATION
        User user = repository.findById(USER_ID_VALID).get();

        assertThat(user.getStatus()).isEqualTo(UserStatus.DISABLE);

        // NOTIFICATION VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserDeleted());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(userResponse.getId()).isEqualTo(String.valueOf(1));
        assertThat(userResponse.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Usuário 01");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.DISABLE.toString());
        assertThat(userResponse.getCreatedAt()).isNotNull();

        assertThat(brokerResponse.getHeaders().get("event_name")).isEqualTo("UserDeletion");
    }

    @Test
    @DisplayName("I want to delete a user that does not exist")
    public void deleteUserThatDoesNotExist() {
        ResponseEntity<DefaultErrorResponse> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.DELETE,
                        HttpEntity.EMPTY,
                        DefaultErrorResponse.class,
                        USER_ID_NOT_FOUND);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
        assertThat(response.getBody().getDetail()).isEqualTo("User not found");
        assertThat(response.getBody().getType()).isNull();

        assertThat(collector.forChannel(brokerOutput.publishUserUpdated())).isNull();
    }
}
