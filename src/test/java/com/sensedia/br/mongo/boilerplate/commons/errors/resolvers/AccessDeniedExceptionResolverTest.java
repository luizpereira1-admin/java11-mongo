package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.access.AccessDeniedException;

import static org.junit.jupiter.api.Assertions.*;

class AccessDeniedExceptionResolverTest {

    private AccessDeniedExceptionResolver accessDeniedExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.accessDeniedExceptionResolver = new AccessDeniedExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        AccessDeniedException exception = Mockito.mock(AccessDeniedException.class);
        Mockito.when(exception.getMessage()).thenReturn("Access Denied");

        DefaultErrorResponse<?> defaultErrorResponse = this.accessDeniedExceptionResolver.getErrorResponse(exception);

        assertEquals("Access Denied", defaultErrorResponse.getDetail());
        assertEquals(401, defaultErrorResponse.getStatus());
    }

}
