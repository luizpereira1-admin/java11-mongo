package com.sensedia.br.mongo.boilerplate.user.domain;

import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class UserStatusTest {

    @Test
    void fromValueReturnObjectWithStringInformed() {
        assertEquals(UserStatus.DISABLE, UserStatus.fromValue("DISABLE"));
    }

    @Test
    void fromValueReturnNullIfBlankValueInformed() {
        assertNull(UserStatus.fromValue(""));
    }

    @Test
    void fromValueThrowsBadRequestExceptionWhenValueInformedNotValid() {
        assertThrows(BadRequestException.class, () -> UserStatus.fromValue("Invalid"));
    }

    @Test
    void valuesReturnStringWithValuesOfStatus() {
        assertEquals("[ACTIVE, DISABLE]", Arrays.toString(UserStatus.values()));
    }

    @Test
    void valueOfReturnObjectWithStringInformed() {
        assertEquals(UserStatus.ACTIVE, UserStatus.valueOf("ACTIVE"));
    }
}
