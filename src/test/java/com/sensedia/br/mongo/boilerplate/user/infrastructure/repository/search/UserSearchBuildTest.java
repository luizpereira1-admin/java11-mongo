package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class UserSearchBuildTest {

    private UserSearch userSearch;
    private String name;
    private String email;
    private Integer page;
    private Integer limit;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault());

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.name = faker.name().fullName();
        this.email = faker.internet().safeEmailAddress();
        this.page = faker.number().numberBetween(1, 10);
        this.limit = faker.number().numberBetween(1, 100);

        this.userSearch = UserSearchBuild
                .builder()
                .name(this.name)
                .email(this.email)
                .status("ACTIVE")
                .page(page)
                .limit(limit)
                .createdAtStart(Instant.now())
                .createdAtEnd(Instant.now())
                .sort("NAME")
                .sortType("ASC")
                .build();
    }

    @Test
    void getNameReturnNameInformed() {
        assertEquals(this.name, this.userSearch.getName());
    }

    @Test
    void getEmailReturnEmailInformed() {
        assertEquals(this.email, this.userSearch.getEmail());
    }

    @Test
    void getStatusReturnStatusInformed() {
        assertEquals(UserStatus.ACTIVE, this.userSearch.getStatus());
    }

    @Test
    void getPageReturnPageInformed() {
        assertEquals(this.page, this.userSearch.getPage());
    }

    @Test
    void getLimitReturnLimitInformed() {
        assertEquals(this.limit, this.userSearch.getLimit());
    }

    @Test
    void getCreatedAtEndReturnDateInformed() {
        assertEquals(FORMATTER.format(Instant.now()), FORMATTER.format(this.userSearch.getCreatedAtEnd()));
    }

    @Test
    void getCreatedAtStartReturnDateInformed() {
        assertEquals(FORMATTER.format(Instant.now()), FORMATTER.format(this.userSearch.getCreatedAtStart()));
    }

    @Test
    void getSortReturnSortInformed() {
        assertEquals(Sort.NAME, this.userSearch.getSort());
    }

    @Test
    void getSortTypeReturnSortTypeInformed() {
        assertEquals(SortType.ASC, this.userSearch.getSortType());
    }
}