package com.sensedia.br.mongo.boilerplate.user.features;

import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class HttpUserCreationTest extends AbstractUserTest{

    @BeforeEach
    public void setup() {
        repository.deleteAll();
    }

    @Test
    @DisplayName("I want to create a user with success")
    public void createUserSuccessfully() throws IOException {
        UserCreationDto userCreation = new UserCreationDto(
                "Usuário 01",
                "usuario01@sensedia.com");

        ResponseEntity<UserDto> response = request.exchange("/users", HttpMethod.POST, new HttpEntity<>(userCreation), UserDto.class);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getHeaders().get("Location")).isNotEmpty();

        // DATABASE VALIDATION
        User user = repository.findAll().iterator().next();

        assertThat(user.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(user.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(user.getName()).isEqualTo("Usuário 01");
        assertThat(user.getStatus()).isEqualTo(UserStatus.ACTIVE);
        assertThat(user.getCreatedAt()).isNotNull();
        assertThat(user.getUpdatedAt()).isNotNull();

        // EVENT VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserCreated());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(userResponse.getId()).isGreaterThanOrEqualTo(String.valueOf(1));
        assertThat(userResponse.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Usuário 01");
        assertThat(userResponse.getStatus()).isEqualTo(String.valueOf(UserStatus.ACTIVE));
        assertThat(userResponse.getCreatedAt()).isNotNull();
        assertThat(userResponse.getUpdatedAt()).isNotNull();

        assertThat(brokerResponse.getHeaders().get("event_name")).isEqualTo("UserCreation");
    }

    @Test
    @DisplayName("I want to create a user without name")
    public void createUserWithoutName() {
        UserCreationDto userCreation = new UserCreationDto(
                null,
                "usuario01@sensedia.com");

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange("/users", HttpMethod.POST, new HttpEntity<>(userCreation), DefaultErrorResponse.class);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
        assertThat(response.getBody().getDetail()).isEqualTo("name é obrigatório ou está no formato inválido.");
        assertThat(response.getBody().getType()).isNull();

        assertThat(repository.findAll()).hasSize(0);
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }

    @Test
    @DisplayName("I want to create a user without email")
    public void createUserWithoutEmail() {
        UserCreationDto userCreation = new UserCreationDto(
                "Usuário 01",
                null);

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange("/users", HttpMethod.POST, new HttpEntity<>(userCreation), DefaultErrorResponse.class);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
        assertThat(response.getBody().getDetail()).isEqualTo("email é obrigatório ou está no formato inválido.");
        assertThat(response.getBody().getType()).isNull();

        assertThat(repository.findAll()).hasSize(0);
        assertThat(collector.forChannel(brokerOutput.publishUserCreated())).isNull();
    }
}
