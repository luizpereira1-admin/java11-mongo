package com.sensedia.br.mongo.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDeletionDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.io.IOException;

import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.APP_ID_HEADER_NAME;
import static com.sensedia.br.mongo.boilerplate.commons.headers.DefaultHeader.EVENT_NAME_HEADER_HEADER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class AmqpUserDeletionTest extends AbstractUserTest {

    @BeforeEach
    public void setup() throws IOException {
        repository.deleteAll();
        loadDatabase();
    }

    @Test
    @DisplayName("I want to delete a user with success")
    public void deleteUserSuccessfully() throws IOException {
        UserDeletionDto userDeletion = new UserDeletionDto(
                USER_ID_VALID
        );

        Message<UserDeletionDto> message =
                MessageBuilder.withPayload(userDeletion).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserDeletionRequested().send(message);

        // DATABASE VALIDATION
        User user = repository.findById(USER_ID_VALID).get();

        assertThat(user.getStatus()).isEqualTo(UserStatus.DISABLE);

        // NOTIFICATION VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserDeleted());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(userResponse.getId()).isEqualTo(String.valueOf(1));
        assertThat(userResponse.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Usuário 01");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.DISABLE.toString());
        assertThat(userResponse.getCreatedAt()).isNotNull();

        assertThat(brokerResponse.getHeaders().get("event_name")).isEqualTo("UserDeletion");
    }

    @Test
    @DisplayName("I want to delete a user that does not exist")
    public void deleteUserThatDoesNotExist() throws IOException {
        UserDeletionDto userDeletion = new UserDeletionDto(
                USER_ID_NOT_FOUND
        );

        Message<UserDeletionDto> message =
                MessageBuilder.withPayload(userDeletion).setHeader(APP_ID_HEADER_NAME, "app-test-feature").build();

        brokerInput.subscribeUserDeletionRequested().send(message);

        // RESPONSE VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserOperationError());

        DefaultErrorResponse<UserDeletionDto> response =
                brokerResponse.getPayload(new TypeReference<>() {
                });

        assertThat(response.getOriginalMessage()).isEqualTo(userDeletion);
        assertThat(response.getStatus()).isEqualTo(NOT_FOUND.value());
        assertThat(response.getTitle()).isEqualTo(NOT_FOUND.getReasonPhrase());
        assertThat(response.getDetail()).isEqualTo("User not found");
        assertThat(response.getType()).isNull();

        MessageHeaders headers = brokerResponse.getHeaders();

        assertThat(headers.get(EVENT_NAME_HEADER_HEADER)).isEqualTo("UserOperationError");
        assertThat(headers.get(APP_ID_HEADER_NAME)).isEqualTo("mongo-boilerplate");

        // REPOSITORY VALIDATION
        assertThat(repository.findAll()).hasSize(5);

        // NOTIFICATION VALIDATION
        assertThat(collector.forChannel(brokerOutput.publishUserDeleted())).isNull();
    }
}
