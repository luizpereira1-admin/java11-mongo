package com.sensedia.br.mongo.boilerplate.user.infrastructure.dto;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserDtoTest {

    private UserDto userDto;
    private String id;
    private String name;
    private String email;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withZone(ZoneId.systemDefault());

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.id = faker.internet().uuid();
        this.name = faker.name().fullName();
        this.email = faker.internet().safeEmailAddress();

        this.userDto = new UserDto(
                this.id,
                this.name,
                this.email,
                "ACTIVE",
                Instant.now(),
                Instant.now()
        );
    }

    @Test
    void getIdReturnStringIdInformedInInstantiation() {
        assertEquals(this.id, this.userDto.getId());
    }

    @Test
    void getNameReturnStringNameInformedInInstantiation() {
        assertEquals(this.name, this.userDto.getName());
    }

    @Test
    void getEmailReturnStringEmailInformedInInstantiation() {
        assertEquals(this.email, this.userDto.getEmail());
    }

    @Test
    void getStatusReturnStringStatusInformedInInstantiation() {
        assertEquals("ACTIVE", this.userDto.getStatus());
    }

    @Test
    void getCreatedAtReturnSameDateInformedInInstantiation() {
        Instant now = Instant.now();

        assertEquals(FORMATTER.format(now), FORMATTER.format(this.userDto.getCreatedAt()));
    }

    @Test
    void getUpdatedAtReturnSameDateInformedInInstantiation() {
        Instant now = Instant.now();

        assertEquals(FORMATTER.format(now), FORMATTER.format(this.userDto.getUpdatedAt()));
    }
}
