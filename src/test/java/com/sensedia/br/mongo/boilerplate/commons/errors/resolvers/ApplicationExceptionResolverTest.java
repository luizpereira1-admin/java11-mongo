package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.ApplicationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationExceptionResolverTest {

    private ApplicationExceptionResolver applicationExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.applicationExceptionResolver = new ApplicationExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        ApplicationException applicationException = Mockito.mock(ApplicationException.class);
        DefaultErrorResponse<?> defaultErrorResponse = Mockito.mock(DefaultErrorResponse.class);

        Mockito.when(defaultErrorResponse.getDetail()).thenReturn("Error");
        Mockito.when(applicationException.getDefaultErrorResponse()).thenReturn(defaultErrorResponse);

        String detail = this.applicationExceptionResolver.getErrorResponse(applicationException).getDetail();
        assertEquals("Error", detail);
    }
}
