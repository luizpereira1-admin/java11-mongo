package com.sensedia.br.mongo.boilerplate.user.features;

import com.sensedia.br.mongo.boilerplate.commons.BrokerResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class HttpUserUpdateTest extends AbstractUserTest {

    @BeforeEach
    public void setup() throws IOException {
        repository.deleteAll();
        loadDatabase();
    }

    @Test
    @DisplayName("I want to update a user with success")
    public void updateUserSuccessfully() throws IOException {
        UserUpdateDto userUpdateDto = new UserUpdateDto(null, "Thiago Costa", "thiago.costa@sensedia.com");

        ResponseEntity<UserDto> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.PUT,
                        new HttpEntity<>(userUpdateDto),
                        UserDto.class,
                        USER_ID_VALID);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        // DATABASE VALIDATION
        User user = repository.findById(USER_ID_VALID).get();

        assertThat(user.getId()).isEqualTo(USER_ID_VALID);
        assertThat(user.getEmail()).isEqualTo("thiago.costa@sensedia.com");
        assertThat(user.getName()).isEqualTo("Thiago Costa");
        assertThat(user.getStatus()).isEqualTo(UserStatus.ACTIVE);
        assertThat(user.getCreatedAt()).isNotNull();
        assertThat(user.getUpdatedAt()).isNotNull();

        // NOTIFICATION VALIDATION
        BrokerResponse brokerResponse = collector.forChannel(brokerOutput.publishUserUpdated());

        UserDto userResponse = brokerResponse.getPayload(UserDto.class);

        assertThat(userResponse.getId()).isEqualTo(USER_ID_VALID);
        assertThat(userResponse.getEmail()).isEqualTo("thiago.costa@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Thiago Costa");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.ACTIVE.toString());
        assertThat(user.getCreatedAt()).isNotNull();
        assertThat(userResponse.getUpdatedAt()).isNotNull();

        assertThat(brokerResponse.getHeaders().get("event_name")).isEqualTo("UserUpdate");
    }

    @Test
    @DisplayName("I want to update a user without email")
    public void updateUserWithoutEmail() {
        UserUpdateDto userUpdateDto = new UserUpdateDto(null, "Thiago Costa", null);

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.PUT,
                        new HttpEntity<>(userUpdateDto),
                        DefaultErrorResponse.class,
                        USER_ID_VALID);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
        assertThat(response.getBody().getDetail())
                .isEqualTo("email é obrigatório ou está no formato inválido.");
        assertThat(response.getBody().getType()).isNull();

        assertThat(repository.findById(USER_ID_VALID).get().getName()).isEqualTo("Usuário 01");
        assertThat(repository.findById(USER_ID_VALID).get().getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(collector.forChannel(brokerOutput.publishUserUpdated())).isNull();
    }

    @Test
    @DisplayName("I want to update a user without name")
    public void updateUserWithoutName() {
        UserUpdateDto userUpdateDto = new UserUpdateDto(null, null, "thiago.costa@sensedia.com");

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.PUT,
                        new HttpEntity<>(userUpdateDto),
                        DefaultErrorResponse.class,
                        USER_ID_VALID);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
        assertThat(response.getBody().getDetail())
                .isEqualTo("name é obrigatório ou está no formato inválido.");
        assertThat(response.getBody().getType()).isNull();

        assertThat(repository.findById(USER_ID_VALID).get().getName()).isEqualTo("Usuário 01");
        assertThat(repository.findById(USER_ID_VALID).get().getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(collector.forChannel(brokerOutput.publishUserUpdated())).isNull();
    }

    @Test
    @DisplayName("I want to update a user with invalid email")
    public void updateUserWithInvalidEmail() {
        UserUpdateDto userUpdateDto = new UserUpdateDto(null, "Thiago Costa", "thiago.costa.sensedia.com");

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.PUT,
                        new HttpEntity<>(userUpdateDto),
                        DefaultErrorResponse.class,
                        USER_ID_VALID);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.BAD_REQUEST.getReasonPhrase());
        assertThat(response.getBody().getDetail()).isEqualTo("email deve ser um endereço de e-mail bem formado");
        assertThat(response.getBody().getType()).isNull();

        assertThat(repository.findById(USER_ID_VALID).get().getName()).isEqualTo("Usuário 01");
        assertThat(repository.findById(USER_ID_VALID).get().getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(collector.forChannel(brokerOutput.publishUserUpdated())).isNull();
    }

    @Test
    @DisplayName("I want to update a user that does not exist")
    public void updateUserThatDoesNotExist() {
        UserUpdateDto userUpdateDto = new UserUpdateDto(null, "Thiago Costa", "thiago.costa@sensedia.com");

        ResponseEntity<DefaultErrorResponse> response =
                request.exchange(
                        "/users/{id}",
                        HttpMethod.PUT,
                        new HttpEntity<>(userUpdateDto),
                        DefaultErrorResponse.class,
                        USER_ID_NOT_FOUND);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
        assertThat(response.getBody().getDetail()).isEqualTo("User not found");
        assertThat(response.getBody().getType()).isNull();

        assertThat(collector.forChannel(brokerOutput.publishUserUpdated())).isNull();
    }
}
