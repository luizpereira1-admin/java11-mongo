package com.sensedia.br.mongo.boilerplate.user.features;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.user.domain.UserStatus;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class HttpUserGetTest extends AbstractUserTest {

    @BeforeEach
    public void setup() throws IOException {
        repository.deleteAll();
        loadDatabase();
    }

    @Test
    @DisplayName("I want to get a user with success")
    public void getUserSuccessfully() {
        ResponseEntity<UserDto> response = request.exchange(
                "/users/{id}", HttpMethod.GET, HttpEntity.EMPTY, UserDto.class, USER_ID_VALID);

        // RESPONSE VALIDATION
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        UserDto userResponse = response.getBody();

        assertThat(userResponse.getId()).isEqualTo(USER_ID_VALID);
        assertThat(userResponse.getEmail()).isEqualTo("usuario01@sensedia.com");
        assertThat(userResponse.getName()).isEqualTo("Usuário 01");
        assertThat(userResponse.getStatus()).isEqualTo(UserStatus.ACTIVE.toString());
        assertThat(userResponse.getCreatedAt()).isNotNull();
        assertThat(userResponse.getUpdatedAt()).isNotNull();
    }

    @Test
    @DisplayName("I want to get a user that does not exist")
    public void getUserThatDoesNotExist() {
        ResponseEntity<DefaultErrorResponse> response = request.exchange(
                "/users/{id}", HttpMethod.GET, HttpEntity.EMPTY, DefaultErrorResponse.class, USER_ID_NOT_FOUND);

        // RESPONSE VALIDATION
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        Assertions.assertThat(response.getBody().getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        Assertions.assertThat(response.getBody().getTitle()).isEqualTo(HttpStatus.NOT_FOUND.getReasonPhrase());
        Assertions.assertThat(response.getBody().getDetail()).isEqualTo("User not found");
        Assertions.assertThat(response.getBody().getType()).isNull();
    }
}
