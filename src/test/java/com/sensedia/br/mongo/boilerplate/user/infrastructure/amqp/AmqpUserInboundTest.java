package com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import com.sensedia.br.mongo.boilerplate.commons.errors.resolvers.ExceptionResolver;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserCreationDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserDeletionDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.dto.UserUpdateDto;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.AmqpPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.ApplicationPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.fail;

class AmqpUserInboundTest {

    private ApplicationPort applicationPort;
    private AmqpUserInbound amqpUserInbound;
    private Faker faker;

    @BeforeEach
    void setUp() {
        this.faker = new Faker();
        MockitoAnnotations.openMocks(this);

        this.applicationPort = Mockito.mock(ApplicationPort.class);
        AmqpPort amqpPort = Mockito.mock(AmqpPort.class);
        ExceptionResolver exceptionResolver = Mockito.mock(ExceptionResolver.class);
        DefaultErrorResponse<?> defaultErrorResponse = Mockito.mock(DefaultErrorResponse.class);

        Mockito.when(this.applicationPort.create(Mockito.any(User.class))).thenReturn(null);
        Mockito.when(this.applicationPort.update(Mockito.any(User.class), Mockito.anyString())).thenReturn(null);
        Mockito.when(defaultErrorResponse.addOriginalMessage(Mockito.any(Object.class))).thenReturn(defaultErrorResponse);
        Mockito.when(exceptionResolver.solve(Mockito.any(Throwable.class))).thenReturn(defaultErrorResponse);

        this.amqpUserInbound = new AmqpUserInbound(this.applicationPort, amqpPort, exceptionResolver);
    }

    @Test
    void whenRunningSubscribeExchangeUserCreationRequestedNotThrowsException() {
        try {
            this.amqpUserInbound.subscribeExchangeUserCreationRequested(new UserCreationDto(
                    this.faker.name().fullName(),
                    this.faker.internet().safeEmailAddress()
            ));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningSubscribeExchangeUserCreationRequestedGenerateAMQPError() {
        Mockito.when(applicationPort.create(Mockito.any(User.class))).thenThrow();

        try {
            this.amqpUserInbound.subscribeExchangeUserCreationRequested(new UserCreationDto(
                    this.faker.name().fullName(),
                    this.faker.internet().safeEmailAddress()
            ));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningSubscribeExchangeUserDeletionRequestedNotThrowsException() {
        try {
            this.amqpUserInbound.subscribeExchangeUserDeletionRequested(
                    new UserDeletionDto(this.faker.internet().uuid())
            );
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningSubscribeExchangeUserDeletionRequestedGenerateAMQPError() {
        Mockito.doThrow(new BadRequestException()).when(applicationPort).delete(Mockito.anyString());

        try {
            this.amqpUserInbound.subscribeExchangeUserDeletionRequested(
                    new UserDeletionDto(this.faker.internet().uuid())
            );
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningSubscribeExchangeUserUpdateRequestedNotThrowsException() {
        try {
            this.amqpUserInbound.subscribeExchangeUserUpdateRequested(new UserUpdateDto(
                    this.faker.internet().uuid(),
                    this.faker.name().fullName(),
                    this.faker.internet().safeEmailAddress()
            ));
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void whenRunningSubscribeExchangeUserUpdateRequestedGenerateAMQPError() {
        Mockito.when(applicationPort.update(Mockito.any(User.class), Mockito.anyString())).thenThrow();

        try {
            this.amqpUserInbound.subscribeExchangeUserUpdateRequested(
                    new UserUpdateDto(
                            this.faker.internet().uuid(),
                            this.faker.name().fullName(),
                            this.faker.internet().safeEmailAddress()
                    )
            );
        } catch (Exception exception) {
            fail();
        }
    }
}
