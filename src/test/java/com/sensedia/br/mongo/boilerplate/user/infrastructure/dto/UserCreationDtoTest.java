package com.sensedia.br.mongo.boilerplate.user.infrastructure.dto;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserCreationDtoTest {

    private UserCreationDto userCreationDto;
    private String name;
    private String email;

    @BeforeEach
    void setUp() {
        Faker faker = new Faker();

        this.name = faker.name().fullName();
        this.email = faker.internet().safeEmailAddress();

        this.userCreationDto = new UserCreationDto(this.name, this.email);
    }

    @Test
    void getNameReturnStringNameInformedInInstantiation() {
        assertEquals(this.name, this.userCreationDto.getName());
    }

    @Test
    void getEmailReturnStringNameInformedInInstantiation() {
        assertEquals(this.email, this.userCreationDto.getEmail());
    }

    @Test
    void toUserReturnUserWithInformedData() {
        User user = this.userCreationDto.toUser();

        assertEquals(this.name, user.getName());
        assertEquals(this.email, user.getEmail());
    }
}
