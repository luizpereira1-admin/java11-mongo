package com.sensedia.br.mongo.boilerplate.user.features;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sensedia.br.mongo.boilerplate.commons.MessageCollectorCustom;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp.config.BrokerInput;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.amqp.config.BrokerOutput;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.RepositoryPort;
import com.sensedia.br.mongo.boilerplate.user.service.UserService;
import io.micrometer.core.instrument.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.mockito.Mockito.mock;

public class AbstractUserTest {

    static final String USER_ID_VALID = "1";

    static final String USER_ID_NOT_FOUND = "999";

    @Autowired
    RepositoryPort repository;

    @Autowired
    TestRestTemplate request;

    @Autowired
    MessageCollectorCustom collector;

    @Autowired
    BrokerOutput brokerOutput;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    private UserService userApplication;

    @Autowired
    BrokerInput brokerInput;

    @Value("classpath:users.json")
    private org.springframework.core.io.Resource usersJson;

    void loadDatabase() throws IOException {
        String json = loadDate(usersJson);
        List<User> users = mapper.readValue(json, new TypeReference<List<User>>() {});
        repository.saveAll(users);
    }

    String loadDate(Resource resource) throws IOException {
        return IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);
    }

    void injectDatabaseError() {
        RepositoryPort failingRepository = mock(RepositoryPort.class);
        ReflectionTestUtils.setField(userApplication, "repository", failingRepository);
    }

    void undoDatabaseError() {
        ReflectionTestUtils.setField(userApplication, "repository", repository);
    }
}
