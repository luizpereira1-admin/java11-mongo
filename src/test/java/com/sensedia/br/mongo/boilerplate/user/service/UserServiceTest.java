package com.sensedia.br.mongo.boilerplate.user.service;

import com.github.javafaker.Faker;
import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.NotFoundException;
import com.sensedia.br.mongo.boilerplate.user.domain.User;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.AmqpPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.ports.RepositoryPort;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearch;
import com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search.UserSearchResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    private UserService userService;
    private RepositoryPort repositoryPort;
    private Faker faker;
    private User user;
    private List<User> users;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.faker = new Faker();

        this.repositoryPort = Mockito.mock(RepositoryPort.class);
        AmqpPort amqpPort = Mockito.mock(AmqpPort.class);
        UserSearchResponse userSearchResponse = Mockito.mock(UserSearchResponse.class);
        this.users = List.of(
                Mockito.mock(User.class),
                Mockito.mock(User.class),
                Mockito.mock(User.class)
        );

        this.user = new User(this.faker.internet().uuid(),"Test", "test@email.com");

        Mockito.when(userSearchResponse.getUsers()).thenReturn(this.users);
        Mockito.when(userSearchResponse.getTotal()).thenReturn(this.users.size());
        Mockito.when(userSearchResponse.getMaximumLimitPerPage()).thenReturn(100);
        Mockito.when(this.repositoryPort.findById(Mockito.anyString())).thenReturn(Optional.of(this.user));
        Mockito.when(this.repositoryPort.findAll(Mockito.any(UserSearch.class))).thenReturn(userSearchResponse);

        this.userService = new UserService(this.repositoryPort, amqpPort);
    }

    @Test
    void createReturnSameUserInformedInParameter() {
        User user = new User(
                this.faker.internet().uuid(),
                this.faker.name().fullName(),
                this.faker.internet().safeEmailAddress()
        );

        User userCreate = this.userService.create(user);

        assertEquals(userCreate, user);
    }

    @Test
    void whenRunningDeleteNotThrowsException() {
        try {
            this.userService.delete(faker.internet().uuid());
        } catch (Exception exception) {
            fail();
        }
    }

    @Test
    void updateChangeNameAndEmailForInformed() {
        String name = this.faker.name().fullName();
        String email = this.faker.internet().safeEmailAddress();

        User userUpdate = this.userService.update(new User(null, name, email), this.faker.internet().uuid());

        assertEquals(name, userUpdate.getName());
        assertEquals(email, userUpdate.getEmail());
    }

    @Test
    void findByIdReturnUserWithoutException() {
        User user = this.userService.findById(this.faker.internet().uuid());
        assertEquals(this.user, user);
    }

    @Test
    void findByIdThrowsNotFoundExceptionWhenNotFoundUserWithId() {
        Mockito.when(this.repositoryPort.findById(Mockito.anyString())).thenThrow(NotFoundException.class);

        assertThrows(NotFoundException.class, () -> this.userService.findById(this.faker.internet().uuid()));
    }

    @Test
    void findAllReturnUserResponseWithInformedUserSearch() {
        UserSearchResponse response = this.userService.findAll(Mockito.mock(UserSearch.class));

        assertEquals(this.users.size(), response.getUsers().size());
        assertEquals(this.users.size(), response.getTotal());
        assertEquals(100, response.getMaximumLimitPerPage());
    }
}
