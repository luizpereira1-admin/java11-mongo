package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IllegalArgumentExceptionResolverTest {

    private IllegalArgumentExceptionResolver illegalArgumentExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.illegalArgumentExceptionResolver = new IllegalArgumentExceptionResolver();
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponse() {
        IllegalArgumentException exception = Mockito.mock(IllegalArgumentException.class);
        Mockito.when(exception.getMessage()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.illegalArgumentExceptionResolver.getErrorResponse(exception);

        assertEquals("Error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}