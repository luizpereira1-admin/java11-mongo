package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search;

import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SortTypeTest {
    @Test
    void valuesReturnStringWithValuesOfSortType() {
        assertEquals("[ASC, DESC]", Arrays.toString(SortType.values()));
    }

    @Test
    void valueOfReturnObjectWithStringInformed() {
        assertEquals(SortType.ASC, SortType.valueOf("ASC"));
    }

    @Test
    void fromValueReturnObjectWithStringInformed() {
        assertEquals(SortType.ASC, SortType.fromValue("ASC"));
    }

    @Test
    void fromValueReturnNullIfBlankValueInformed() {
        assertNull(SortType.fromValue(""));
    }

    @Test
    void fromValueThrowsBadRequestExceptionWhenValueInformedNotValid() {
        assertThrows(BadRequestException.class, () -> SortType.fromValue("Invalid"));
    }

    public static Object[][] getValueDataProvider() {
        return new Object[][] {
                {"asc", SortType.ASC},
                {"desc", SortType.DESC}
        };
    }

    @ParameterizedTest
    @MethodSource("getValueDataProvider")
    void getValueReturnNameOfSortTypeAccordingToTheInstantiatedSortType(String expected, SortType sortType) {
        assertEquals(expected, sortType.getValue());
    }
}
