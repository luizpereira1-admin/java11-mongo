package com.sensedia.br.mongo.boilerplate.user.infrastructure.repository.search;

import com.sensedia.br.mongo.boilerplate.commons.errors.exceptions.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SortTest {
    @Test
    void valuesReturnStringWithValuesOfSort() {
        assertEquals("[NAME, EMAIL, STATUS, CREATED_AT]", Arrays.toString(Sort.values()));
    }

    @Test
    void valueOfReturnObjectWithStringInformed() {
        assertEquals(Sort.NAME, Sort.valueOf("NAME"));
    }

    @Test
    void fromValueReturnObjectWithStringInformed() {
        assertEquals(Sort.NAME, Sort.fromValue("NAME"));
    }

    @Test
    void fromValueReturnNullIfBlankValueInformed() {
        assertNull(Sort.fromValue(""));
    }

    @Test
    void fromValueThrowsBadRequestExceptionWhenValueInformedNotValid() {
        assertThrows(BadRequestException.class, () -> Sort.fromValue("Invalid"));
    }

    public static Object[][] fieldNameDataProvider() {
        return new Object[][] {
                {"name", Sort.NAME},
                {"email", Sort.EMAIL},
                {"status", Sort.STATUS},
                {"createdAt", Sort.CREATED_AT}
        };
    }

    @ParameterizedTest
    @MethodSource("fieldNameDataProvider")
    void getFieldNameReturnNameOfFieldAccordingToTheInstantiatedSort(String expected, Sort sort) {
        assertEquals(expected, sort.getFieldName());
    }
}
