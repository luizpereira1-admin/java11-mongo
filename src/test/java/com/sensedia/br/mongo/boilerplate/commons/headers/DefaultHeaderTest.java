package com.sensedia.br.mongo.boilerplate.commons.headers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultHeaderTest {

    private DefaultHeader defaultHeader;

    @BeforeEach
    void setUp() {
        this.defaultHeader = new DefaultHeader();
        this.defaultHeader
                .appId("id-test")
                .eventName("event-test")
                .contentRange(1)
                .acceptRange(1);
    }

    @Test
    void whenAppIdIsExecutedPutAppIdHeaderName() {
        assertEquals("id-test", this.defaultHeader.get(DefaultHeader.APP_ID_HEADER_NAME));
    }

    @Test
    void whenEventNameIsExecutedPutEventNameHeaderName() {
        assertEquals("event-test", this.defaultHeader.get(DefaultHeader.EVENT_NAME_HEADER_HEADER));
    }

    @Test
    void whenContentRangeIsExecutedPutHeaderContentRange() {
        assertEquals(1, this.defaultHeader.get(DefaultHeader.HEADER_CONTENT_RANGE));
    }

    @Test
    void whenAcceptRangeIsExecutedPutHeaderAcceptRange() {
        assertEquals(1, this.defaultHeader.get(DefaultHeader.HEADER_ACCEPT_RANGE));
    }
}
