package com.sensedia.br.mongo.boilerplate.commons.errors.resolvers;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.sensedia.br.mongo.boilerplate.commons.errors.domains.DefaultErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonMappingExceptionResolverTest {

    private JsonMappingExceptionResolver jsonMappingExceptionResolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.jsonMappingExceptionResolver = new JsonMappingExceptionResolver();
        ReflectionTestUtils.setField(this.jsonMappingExceptionResolver, "invalidFieldMessage", "error");
    }

    @Test
    void getErrorResponseReturnDefaultErrorResponseWithJsonMappingException() {
        JsonMappingException.Reference reference = Mockito.mock(JsonMappingException.Reference.class);
        JsonMappingException exception = Mockito.mock(JsonMappingException.class);

        Mockito.when(reference.getFieldName()).thenReturn("test");
        Mockito.when(exception.getPath()).thenReturn(List.of(reference));
        Mockito.when(exception.getMessage()).thenReturn("Error");

        DefaultErrorResponse<?> defaultErrorResponse = this.jsonMappingExceptionResolver.getErrorResponse(exception);

        assertEquals("test error", defaultErrorResponse.getDetail());
        assertEquals(400, defaultErrorResponse.getStatus());
    }
}